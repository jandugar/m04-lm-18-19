## Per què presentacions HTML?

* No malbaratar el temps amb eines ofimàtiques
    * partim d'un markdown!
* Capacitat de tenir una presentació en format pla
* Evitar dependències cap al programari ofimàtic

#
## Quines eines necessitem?

* qualsevol editor per **markdown**
* pandoc
* Un navegador d'Internet

#
## Pandoc

[Pandoc](https://pandoc.org/) és una llibreria _Haskell_ usada per convertir fitxers d'un format a d'altre.

S'executa com una ordre en consola amb una sèrie d'opcions.

#
## Utilitat de pandoc

- Conversió entre formats
  > html, markdown, docx, odt, PDF

- Producció de presentacions amb slidy, revealjs,  beamer, ...

#
## Paquets a instal·lar

* [pandoc](https://pandoc.org/ "Pàgina oficial de Pandoc")
* aspell, aspell-ca, aspell-es, ...
* texlive

```
dnf -y install pandoc aspell texlive
pandoc --help
```

#
## aspell: Repassar ortografia

Molt important: revisem l'ortografia de la documentació
```
    aspell --lang=ca check README.md
```

#
## Exemples

```
pandoc -s --toc -c pandoc.css -A footer.html MANUAL.txt -o example3.html

pandoc -s  aux/header2.txt pandoc.md -t slidy -o presentacio.html

pandoc -s  aux/header.yml pandoc.md  -t slidy -o presentacio.html

pandoc -t revealjs -s aux/header.yml pandoc.md -o presentacio.html   -V revealjs-url=https://revealjs.com -V theme=serif -V transition=fade


```
[Fes clic aquí per demos](https://pandoc.org/demos.html)


#
## Presentacions HTML

* Usarem un format de passi de presentacions [slidy](http://www.w3.org/Talks/Tools/Slidy) o [revealjs](http://lab.hakim.se/reveal-js/) entre d'[altres](https://pandoc.org/).

* A partir d'un fitxer en format markdown, li passarem el pandoc

#
## Exemples presentacions HTML
```
pandoc --self-contained  prueba.md -t slidy -o output.html

pandoc -s --toc pandoc.md  aux/header.yml -t slidy -o presentacio.html

pandoc -t revealjs -s aux/header.yml pandoc.md -o presentacio.html -V revealjs-url=https://revealjs.com -V theme=serif -V transition=fade


```
#
## Opcions bàsiques:

* -f (from) (en casos molt especials, habitualment no s'usa)
* -t (to)
* -o (output)
* -s,--standalone, versió lleugera, fa crida als estils.
* --self-contained: versió pesada, descarrega tot el necessari al codi HTML

#
## Opcions bàsiques:
* --toc
* fitxer.yml
* -c styles.css
* -i  
    els ítems de les llistes apareixeran de manera incremental (no el feu servir!)

#
## Fer passi de diapositives

Només carregar el fitxer HTML amb un navegador

#
## Dreceres amb slidy

* C: mostrar la toc
* S: Fer petita la font
* B: Augmenta la font  

![](aux/logo.png)

#
## Exemples de fitxers de metadades
* Bloc de títol:
```
% Markdown, Pandoc i presentacions HTML
% Jordi Andúgar
% 27 de juny 2019
```

Format YAML:
```
---
title: Markdown, Pandoc i presentacions HTML
author: Jordi Andúgar
...
```

#
## Vull afegir CSS!

```
pandoc -s -c aux/style.css aux/header2.txt pandoc.md -t slidy -o presentacio.html
```

#
## Diapositives PDF (Beamer)

* fitxer de metadades (titol, autor, tema, ...)
```
pandoc -t beamer pandoc.md aux/beamer.yml -o pandoc.pdf
```

* Variables en consola(`-V variable=valor`):
```
pandoc -t beamer pandoc.md -V author=Jordi -o pandoc.pdf
```

[Temes de beamer](http://deic.uab.es/~iblanes/beamer_gallery/)  
[Més temes](https://hartwork.org/beamer-theme-matrix/).

#
## Exemples de conversions

Destí|Origen|Ordre
-----|------|-----
PDF|md|pandoc fitxer.md -o fitxer.pdf
md|html|pandoc test.html -o test.md --parse-raw
md|docx|pandoc -s example30.docx -t markdown -o example35.md
pdf|md|pandoc -s 09_DDL.md -t beamer -o 09_DDL.pdf
html|md|pandoc -t slidy --self-contained 09_DDL.md -o prova.html
html|md|pandoc -t slidy --standalone 09_DDL.md -o prova.html

#
## Temes a revealjs

```
-V theme=tema
```

Default - Sky - Beige - Simple - Serif - Night
Moon - Solarized

#
## Transicions a Revealjs
```
 -V transition=transició
```
Cube - Page - Concave - Zoom - Linear - Fade - None - Default

#
## Pràctiques

- Des de consola, proveu les opcions mostrades
- _firefox/chromium_ per executar la presentació HTML
- _evince_ per la presentació pdf
- Esborreu els fitxers HTML i PDF
- Torneu a crear-los amb pandoc i les diferentes opcions
- Investigueu més variables

#
![](aux/cc.png "Licència Creative Commons")
