1. Indiqueu quins són els espais de noms i els prefixos definits en aquest fitxer, explicar perquè serveixen. Mira si en algún cas és possible prescindir d'algun àlies.

```
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    >
    <xsl:template match="doc">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4" page-width="297mm" page-height="210mm" margin-top="1cm" margin-bottom="1cm" margin-left="1cm" margin-right="1cm">
                    <fo:region-body margin="3cm"/>
                    <fo:region-before extent="2cm"/>
                    <fo:region-after extent="2cm"/>
                    <fo:region-start extent="2cm"/>
                    <fo:region-end extent="2cm"/>
                    </fo:simple-page-master>
                </fo:layout-master-set>
            <fo:page-sequence master-reference="A4" format="A">
                <fo:flow flow-name="xsl-region-body">
                    <xsl:apply-templates select="para"/>
                    </fo:flow>
                </fo:page-sequence>
            </fo:root>
    </xsl:template>
    <xsl:template match="para">
        <fo:block><xsl:value-of select="."/></fo:block>
    </xsl:template>
    <xsl:template match="para[1]">
        <fo:block><fo:inline font-weight="bold"><xsl:value-of select="."/></fo:inline></fo:block>
    </xsl:template>
</xsl:stylesheet>
```
