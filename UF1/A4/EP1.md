# CSS i XML

1. Consulteu aquesta [url](http://www.mclibre.org/consultar/xml/lecciones/xml-css.html#css-xml) i proveu els exemples.

2. Donat el codi de sota, doneu-li un format personalitzat, tenint en compte que si el vol és O (verd), D (groc) i C (vermell).
La resta d'estils els triareu vosaltres.


```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE aeroport [
   <!ELEMENT aeroport (nom, vols, data)>
   <!ELEMENT nom (#PCDATA)>
   <!ELEMENT vols (vol+)>
   <!ELEMENT vol (diari?, origen, desti, hora-sortida, hora-arribada)>
      <!ATTLIST vol codi ID #REQUIRED>
      <!ATTLIST vol estat (O|D|C) "O">
   <!ELEMENT diari EMPTY>
   <!ELEMENT origen (#PCDATA)>
   <!ELEMENT desti (#PCDATA)>
   <!ELEMENT hora-sortida (#PCDATA)>
   <!ELEMENT hora-arribada (#PCDATA)>
   <!ELEMENT data (#PCDATA)>
]>

<aeroport>
   <nom>El Prat</nom>
   <vols>
      <vol codi="V22" estat="D">
         <diari>SI</diari>
         <origen>New York</origen>
         <desti>Chicago</desti>
         <hora-sortida>9:30</hora-sortida>
         <hora-arribada>11:30</hora-arribada>
      </vol>
      <vol codi="V23" estat="C">
         <diari>NO</diari>
         <origen>New York</origen>
         <desti>Miami</desti>
         <hora-sortida>10:15</hora-sortida>
         <hora-arribada>11:15</hora-arribada>
      </vol>
   </vols>
   <data>20/12/2013</data>
</aeroport>
```
