#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# Exercici 3.Mostra la lista de provincies i el total de municipis que te cadascuna.

from lxml import etree
arbol = etree.parse ("poblacions.xml")
provincies = arbol.xpath ("/lista/provincia")
for provincia in provincies :
    num_municipis = provincia.xpath("count(localidades/localidad)")
    print "%s: %d Localitats" %(provincia.find("nombre").text, num_municipis)
# És equivalent posar provincia.find("nombre").text que provincia.findtext("nombre")
#   print "%s: %d Localitats" %(provincia.findtext("nombre"), num_municipis)
