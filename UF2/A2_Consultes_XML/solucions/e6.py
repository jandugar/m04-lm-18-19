#!/usr/bin/env python2
# -*- coding: utf-8 -*-
'''
 6. Mostra la província amb més municipis i la que té menys
(província i número de municipis)
'''
from lxml import etree
arbol = etree.parse ("poblacions.xml")
provincies = arbol.xpath ("/lista/provincia")
max = 0
min = 10000
for provincia in provincies :
    num_municipis = provincia.xpath("count(localidades/localidad)")
    if num_municipis > max:
        max = num_municipis
    	provMax = provincia.find("nombre").text
    elif num_municipis < min:
    	min = num_municipis
    	provMin = provincia.find("nombre").text
print 'La província amb més municipis és %s (%d)' % (provMax,max)
print 'La província amb menys municipis és %s (%d)' % (provMin,min)
