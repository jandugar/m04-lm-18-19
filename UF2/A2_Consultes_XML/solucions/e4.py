#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# Exercici 4. Demana per teclat el nom d'una província i mostra els seus municipis.


from lxml import etree
arbre = etree.parse ("poblacions.xml")
provincia = raw_input (" Introdueix una provincia i mostraré totes les seves localitats: ").title ()
localitats = arbre.xpath (u"/lista/provincia[nombre='%s']/localidades/localidad" % provincia)
for localitat in localitats :
    print "%s"  %(localitat.text)
