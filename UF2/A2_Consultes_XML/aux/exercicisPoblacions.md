### Exercicis per fer amb python i XPath (poblacions.xml)

1. Llistat de totes les provincies.
2. Llistat de tots els municipis.
3. Mostra la lista de provincies i el total de municipis que te cadascuna.
4. Demana per teclat el nom d'una província i mostra els seus municipis.
5. Demana per teclat el nom d'un municipi i mostra la provincia on es troba.
6. Mostra la província amb més municipis i la que té menys (província i número de municipis)
